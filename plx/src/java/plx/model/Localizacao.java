/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plx.model;

/**
 *
 * @author alexandre.hauffe
 */
public class Localizacao {
    
    private Integer cep;
    private String bairro;
    private String cidade;
    private String uf;
    
    private Usuario usuario;

    public Localizacao() {
        usuario = new Usuario();
    }
    
    public Integer getCep() {
        return cep;
    }

    public void setCep(Integer cep) {
        this.cep = cep;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }
}
