/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plx.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alexandre.hauffe
 */
public class Categoria {
    
    private Integer id;
    private String nome;
    private String descricao;
    
    private List<Anuncio> anuncios;
    private List<Categoria> categorias;

    public Categoria() {
        anuncios = new ArrayList<Anuncio>();
        categorias = new ArrayList<Categoria>();
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
}
