/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plx.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alexandre.hauffe
 */
public class Usuario {
    
    private String nome;
    private String contato;
    private Integer telefone;
    private String email;
    private String apelido;
    
    private List<Localizacao> localizacoes;
    private List<Anuncio> anuncios;
    

    public Usuario() {
        anuncios = new ArrayList<Anuncio>();
        localizacoes = new ArrayList<Localizacao>();
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    public Integer getTelefone() {
        return telefone;
    }

    public void setTelefone(Integer telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getApelido() {
        return apelido;
    }

    public void setApelido(String apelido) {
        this.apelido = apelido;
    }
    
   
    
}
