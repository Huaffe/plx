package plx.model;

import java.util.Date;

/**
 * @author alexandre.hauffe
 */
public class Anuncio {
    private Date data;
    private String descricao;
    private String titulo;
    private Double preco;
    private String imagem;
    private Integer status;
    
    private Usuario usuario;
    private Categoria categoria;

    public Anuncio() {
        usuario = new Usuario();
        categoria = new Categoria();
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    
    
    
    
}
