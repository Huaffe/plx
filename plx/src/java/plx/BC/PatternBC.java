package plx.BC;

import java.util.List;

public abstract class PatternBC<T> {
    
    public abstract boolean insert(T object);
    public abstract boolean update(T object);
    public abstract boolean delete(Integer id);
    public abstract T findById(Integer id);
    public abstract List<T> findByFilter(String filter);
    public abstract List<T> findAll(Integer id);
    abstract void validade(T object);
}
